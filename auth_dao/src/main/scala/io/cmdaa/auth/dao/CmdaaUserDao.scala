package io.cmdaa.auth.dao

import io.cmdaa.auth.model.{CmdaaUser, UserPhash}
import reactivemongo.api.bson.{BSON, BSONDocument, BSONObjectID, document}
import reactivemongo.api.commands.WriteResult
import reactivemongo.api.indexes.{Index, IndexType}

import javax.inject.{Inject, Singleton}
import scala.concurrent.{ExecutionContext, Future}

// TODO: Later, if needed, we can implement a change email or change username
//       functionality. Those workflows are relatively dangerous - and correct
//       implementation may be non-trivial - so I'm putting them off for now.

@Singleton
class CmdaaUserDao @Inject() (
  override protected val mongoComponents: MongoComponents
) extends MongoDao {
  /** The name of the collection */
  override val colName: String = "cmdaa_users"

  /** Indexes of "cmdaa_users" collection. */
  override protected val indexSeq: Seq[Index.Default] = Seq (
    Index (
      key  = Seq("username" -> IndexType.Ascending),
      name = Some("username_idx"),

      unique     = true,
      background = true,
      sparse     = false
    ),
    Index (
      key  = Seq("email" -> IndexType.Ascending),
      name = Some("email_idx"),

      unique     = true,
      background = true,
      sparse     = false
    ),
    Index (
      key = Seq(
        "s_dn" -> IndexType.Ascending,
        "i_dn" -> IndexType.Ascending
      ),
      name = Some("dn_idx"),

      unique     = true,
      background = true,
      sparse     = true
    )
  )

  /** Fields requested when querying users. */
  private val usrProjection = document (
    "username"  -> 1,
    "email"     -> 1,
    "firstName" -> 1,
    "lastName"  -> 1,
    "meta"      -> 1,
    "i_dn"      -> 1,
    "s_dn"      -> 1,
  )

  /** Fields requested when querying for phash */
  private val phashProjection = document("_id" -> 1, "phash" -> 1)

  /**
   * Inserts a new CmdaaUser from the given model.
   *
   * @param user the user to be inserted
   * @return async WriteResult
   */
  def insert(user: CmdaaUser, phash: String)
    (implicit ec: ExecutionContext)
  : Future[WriteResult] = {
    val userDoc: BSONDocument = BSON.writeDocument(user).get ++
      BSONDocument("phash" -> phash)

    collection flatMap { _.insert.one(userDoc) }
  }

  /**
   * Inserts a new CmdaaUser from the given model. Only meant to be called
   * from PKI Authentication workflows as a password is NOT set.
   *
   * @param user the user to be inserted
   * @param ec   execution context
   * @return asyn WriteResult
   */
  def insertPki(user: CmdaaUser)(implicit ec: ExecutionContext)
  : Future[WriteResult] = {
    collection flatMap { _.insert.one(user) }
  }

  /** Retrieves a user by their ID. */
  def findById(id: BSONObjectID)
    (implicit ec: ExecutionContext)
  : Future[Option[CmdaaUser]] = {
    collection flatMap { _
      .find(document("_id" -> id), Some(usrProjection))
      .one[CmdaaUser]
    }
  }

  /** Retrieves a user by their ID. */
  def findById(id: String)
    (implicit ec: ExecutionContext)
  : Future[Option[CmdaaUser]] = {
    findById(BSONObjectID.parse(id).get)
  }

  /** Retrieves a user by their email */
  def findByEmail(email: String)
    (implicit ec: ExecutionContext)
  : Future[Option[CmdaaUser]] = {
    collection flatMap { _
      .find(document("email" -> email), Some(usrProjection))
      .one[CmdaaUser]
    }
  }

  /** Retrieves a user by their username */
  def findByUsername(username: String)
    (implicit ec: ExecutionContext)
  : Future[Option[CmdaaUser]] = {
    collection flatMap { _
      .find(document("username" -> username), Some(usrProjection))
      .one[CmdaaUser]
    }
  }

  /** Retrieves a user by subject and issuer DN */
  def findByDn(s_dn: String, i_dn: String)
    (implicit ec: ExecutionContext)
  : Future[Option[CmdaaUser]] = {
    collection flatMap { _
      .find(document("s_dn" -> s_dn, "i_dn" -> i_dn), Some(usrProjection))
      .one[CmdaaUser]
    }
  }

  /** Retreives phash by username */
  def getPhash(username: String)
    (implicit ec: ExecutionContext)
  : Future[Option[UserPhash]] = {
    collection flatMap { _
      .find(document("username" -> username), Some(phashProjection))
      .one[UserPhash]
    }
  }

  /**
   * Updates a CmdaaUser's name.
   *
   * @param id       the ID of the record to update
   * @param newFirst the new first name
   * @param newLast  the new last name
   * @return the updated user
   */
  def updateName(id: BSONObjectID, newFirst: String, newLast: String)
    (implicit ec: ExecutionContext)
  : Future[Option[CmdaaUser]] = {
    val qry = document("_id" -> id)
    val upd = document(
      "$set" -> document("firstName" -> newFirst, "lastName" -> newLast),
      "$currentDate" -> document(
        "meta.updated" -> document("$type" -> "date")
      )
    )

    collection flatMap { _
      .findAndUpdate(qry, upd, fetchNewObject = true)
      .map(_.result[CmdaaUser])
    }
  }

  /**
   * Updates a CmdaaUser, adding s_dn and i_dn if those fields aren't already
   * set for the user.
   *
   * @param id   the ID of the user to update
   * @param s_dn value of the subject dn to set
   * @param i_dn value of the issuer dn to set
   * @param ec   execution context
   * @return the updated user
   */
  def updateDn(id: BSONObjectID, s_dn: String, i_dn: String)
    (implicit ec: ExecutionContext)
  : Future[Option[CmdaaUser]] = {
    val qry = document (
      "_id" -> id,
      "s_dn" -> document("$exists" -> false),
      "i_dn" -> document("$exists" -> false)
    )
    val upd = document (
      "$set" -> document("s_dn" -> s_dn, "i_dn" -> i_dn),
      "$currentDate" -> document(
        "meta.updated" -> document("$type" -> "date")
      )
    )

    collection flatMap { _
      .findAndUpdate(qry, upd, fetchNewObject = true)
      .map(_.result[CmdaaUser])
    }
  }

  /**
   * Updates a user's phash.
   *
   * @param id     id of the record
   * @param nPhash new phash
   * @return async write result
   */
  def updatePhash_!(id: BSONObjectID, nPhash: String)
    (implicit ec: ExecutionContext)
  : Future[WriteResult] = {
    val qry = document("_id" -> id)
    val upd = document (
      "$set" -> document("phash" -> nPhash),
      "$currentDate" -> document (
        "meta.updated" -> document("$type" -> "date")
      )
    )

    collection flatMap { _
      .update.one(qry, upd)
    }
  }

  /**
   * Deletes a CmdaaUser by ID.
   *
   * @param id the ID of the user to delete
   * @return async WriteResult
   */
  def delete_!(id: BSONObjectID)
    (implicit ec: ExecutionContext)
  : Future[WriteResult] = {
    collection flatMap { _
      .delete.one(BSONDocument("_id" -> id))
    }
  }
}
