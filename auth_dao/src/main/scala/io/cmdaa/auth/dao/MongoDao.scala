package io.cmdaa.auth.dao

import reactivemongo.api.bson.collection.BSONCollection
import reactivemongo.api.indexes.Index

import scala.concurrent.{ExecutionContext, Future}

trait MongoDao {
  def colName: String

  protected def indexSeq: Seq[Index.Default]
  protected def mongoComponents: MongoComponents

  /**
   * Resolves the collection.
   *
   * @return async BsonCollection
   */
  protected def collection(implicit ec: ExecutionContext)
  : Future[BSONCollection] = {
    mongoComponents.database
      .map(_.collection[BSONCollection](colName))
  }

  /**
   * Initializes the collection. Meant to be safe to call even if the
   * collection already exists and is configured.
   *
   * @return async success boolean
   */
  def initCollection()
    (implicit ex: ExecutionContext)
  : Future[Unit] = {
    collection flatMap { c =>
      Future
        .sequence(indexSeq.map(c.indexesManager.ensure))
        .map(_ => {})
    }
  }
}
