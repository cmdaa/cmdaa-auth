package io.cmdaa.auth.dao

import io.cmdaa.auth.model.SessionToken
import reactivemongo.api.bson.{BSONDateTime, BSONObjectID, document}
import reactivemongo.api.commands.WriteResult
import reactivemongo.api.indexes.Index.Default
import reactivemongo.api.indexes.{Index, IndexType}

import javax.inject.{Inject, Singleton}
import scala.concurrent.{ExecutionContext, Future}

@Singleton
class SessionTokenDao @Inject() (
  override protected val mongoComponents: MongoComponents
) extends MongoDao {
  override val colName: String = "sess_tokens"

  override protected def indexSeq: Seq[Default] = Seq(
    Index (
      key  = Seq("userId" -> IndexType.Ascending),
      name = Some("user_idx"),

      unique     = false,
      background = true,
      sparse     = false
    ),
    Index (
      key  = Seq("token" -> IndexType.Ascending),
      name = Some("token_idx"),

      unique     = true,
      background = true,
      sparse     = false
    )
  )

  /** Inserts a new SessionToken */
  def insert(token: SessionToken)
    (implicit ec: ExecutionContext)
  : Future[WriteResult] =
    collection flatMap { _
      .insert.one(token)
    }

  /** Fetches a SessionToken by its token */
  def findByToken(token: String)
    (implicit ec: ExecutionContext)
  : Future[Option[SessionToken]] =
    collection flatMap { _
      .find(document("token" -> token))
      .one[SessionToken]
    }

  /** Lists SessionTokens by userId. */
  def listByUser(userId: BSONObjectID)
    (implicit ec: ExecutionContext)
  : Future[List[SessionToken]] = {
    collection flatMap { _
      .find(document("userId" -> userId))
      .cursor[SessionToken]().collect[List]()
    }
  }

  /** Access a Token */
  def access(token: String, expireMillis: Long)
    (implicit ec: ExecutionContext)
  : Future[Option[SessionToken]] = {
    val cMillis = System.currentTimeMillis()

    val qry = document (
      "token" -> token,
      "lastAccess" -> document(
        "$gte" -> BSONDateTime(cMillis-expireMillis)
      )
    )

    val upd = document(
      "$set" -> document(
        "lastAccess" -> BSONDateTime(System.currentTimeMillis())
      )
    )

    collection flatMap { _
      .findAndUpdate(qry, upd, fetchNewObject = true)
      .map(_.result[SessionToken])
    }
  }

  /** Deletes a SessionToken by its token */
  def invalidate(token: String)
    (implicit ec: ExecutionContext)
  : Future[WriteResult] =
    collection flatMap { _
      .delete.one(document("token" -> token))
    }

  /** Deletes a SessionToken by its ID */
  def invalidate(id: BSONObjectID)
    (implicit ec: ExecutionContext)
  : Future[WriteResult] =
    collection flatMap { _
      .delete.one(document("_id" -> id))
    }

  /** Cleared expired session tokens from the database */
  def clearExpired(expireMillis: Long)
    (implicit ec: ExecutionContext)
  : Future[WriteResult] = {
    val cMillis = System.currentTimeMillis()
    val qry = document("lastAccess" -> document(
      "$lt" -> BSONDateTime(cMillis-expireMillis)
    ))

    collection flatMap { _.delete.one(qry) }
  }
}
