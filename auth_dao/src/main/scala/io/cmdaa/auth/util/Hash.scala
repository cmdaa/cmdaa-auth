package io.cmdaa.auth.util

import java.security.SecureRandom
import javax.crypto.SecretKeyFactory
import javax.crypto.spec.PBEKeySpec

object Hash {
  /** Tasty hash browns. */
  def saltedHash(str: String): String = {
    val salt = SecureRandom.getInstance("SHA1PRNG").generateSeed(32)
    bytes2hex(salt)+"$"+hash(str, salt)
  }

  /** Compares input with stored hash */
  def check(str: String, shash: String): Boolean = {
    val saltAndPass = shash.split("\\$")

    if (saltAndPass.length != 2)
      throw new IllegalArgumentException("Invalid stored password format.")

    val hashOfInput = hash(str, hex2bytes(saltAndPass(0)))
    hashOfInput == saltAndPass(1)
  }

  /** Hash strs with salt */
  private def hash(str: String, salt: Array[Byte]): String = {
    if (str == null || str.isEmpty)
      throw new IllegalArgumentException("Empty passwords are not supported")

    val spec = new PBEKeySpec(str.toCharArray, salt, 25600, 256)
    val skf = SecretKeyFactory.getInstance("PBKDF2WithHmacSHA1")
    val hash = skf.generateSecret(spec).getEncoded

    bytes2hex(hash)
  }

  /** Got hex? Want bytes? Call this. */
  private def hex2bytes(hex: String): Array[Byte] =
    hex.replaceAll("[^0-9A-Fa-f]", "")
      .sliding(2, 2).toArray
      .map(Integer.parseInt(_, 16).toByte)

  /** Got bytes? Want hex? Call this. */
  private def bytes2hex(bytes: Array[Byte]): String =
    bytes.map("%02x".format(_)).mkString
}
