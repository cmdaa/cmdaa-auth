package io.cmdaa.auth.model

import reactivemongo.api.bson.{
  BSONDateTime, BSONDocumentHandler, BSONObjectID, Macros
}

import java.util.UUID

/**
 * Record of the SessionToken provided to the user for authenticated access
 *
 * @param _id        unique id
 * @param userId     the user's id
 * @param lastAccess the last time this token was used to access cmdaa
 * @param token      the token
 */
case class SessionToken (
  _id:        BSONObjectID,
  userId:     BSONObjectID,
  lastAccess: BSONDateTime,
  token:      String
)
object SessionToken {
  /** BSON de/serializer for SessionToken */
  implicit val handler: BSONDocumentHandler[SessionToken] = Macros.handler

  /** Generates a new session token */
  def genToken(userId: BSONObjectID): SessionToken =
    SessionToken(
      BSONObjectID.generate(),
      userId,
      BSONDateTime(System.currentTimeMillis()),
      UUID.randomUUID().toString
    )
}
