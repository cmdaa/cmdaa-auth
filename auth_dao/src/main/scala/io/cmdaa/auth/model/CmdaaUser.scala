package io.cmdaa.auth.model

import reactivemongo.api.bson.{
  BSONDateTime, BSONDocumentHandler, BSONObjectID, Macros
}

/**
 * BSON Document "schema" for CMDAA Users.
 *
 * @param username  the user's "handle" or login name
 * @param firstName the user's first name
 * @param lastName  the user's last name
 * @param email     the user's email address
 * @param _id       the user's ID
 */
case class CmdaaUser (
  _id:       BSONObjectID,
  meta:      UserMeta,
  username:  String,
  email:     String,
  firstName: String,
  lastName:  String,
  s_dn:      Option[String],
  i_dn:      Option[String]
)

object CmdaaUser {
  /** BSON de/serializer for CmdaaUser */
  implicit val handler: BSONDocumentHandler[CmdaaUser] = Macros.handler

  /** Creates a new CmdaaUser */
  def newUser (
    username:  String,
    email:     String,
    firstName: String,
    lastName:  String,
    s_dn: Option[String] = None,
    i_dn: Option[String] = None
  ): CmdaaUser = {
    val id = BSONObjectID.generate()
    CmdaaUser(id, UserMeta(id),
      username, email, firstName, lastName,
      s_dn, i_dn)
  }
}

/**
 * CmdaaUser document metadata.
 *
 * @param updatedBy the ID of the user who last updated the document.
 * @param created   date/timestamp of when the document was created
 * @param updated   date/timestamp of when the document was last updated
 * @param schema    schema version (just in case)
 */
case class UserMeta (
  updatedBy: BSONObjectID,
  created:   BSONDateTime = BSONDateTime(System.currentTimeMillis()),
  updated:   BSONDateTime = BSONDateTime(System.currentTimeMillis()),
  schema:    Int = 1
)
object UserMeta {
  /** BSON de/serializer for UserMeta */
  implicit val handler: BSONDocumentHandler[UserMeta] = Macros.handler
}


case class UserPhash(_id: BSONObjectID, phash: String)
object UserPhash {
  implicit val handler: BSONDocumentHandler[UserPhash] = Macros.handler
}
