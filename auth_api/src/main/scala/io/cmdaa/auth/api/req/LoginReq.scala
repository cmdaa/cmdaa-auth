package io.cmdaa.auth.api.req

import play.api.libs.json.{Format, Json}

/** Encapsulates data for LoginReq */
case class LoginReq(username: String, password: String)

object LoginReq {
  /** JSON de/serializer for LoginReq */
  implicit val format: Format[LoginReq] = Json.format
}
