package io.cmdaa.auth.api

import play.api.libs.json.{Format, Json}

/** Authentication token for CMDAA Clients */
case class SessionToken (
  id:         String,
  userId:     String,
  lastAccess: String,
  token:      String
)

object SessionToken {
  /** Play Json De/serializer for SessionToken */
  implicit val format: Format[SessionToken] = Json.format
}
