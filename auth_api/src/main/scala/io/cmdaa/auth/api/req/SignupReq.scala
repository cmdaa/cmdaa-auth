package io.cmdaa.auth.api.req

import play.api.libs.json.{Format, Json}

/** Encapsulates data for the Create User Request. */
case class SignupReq (
  username:  String,
  email:     String,
  password:  Option[String],
  firstName: String,
  lastName:  String
)

object SignupReq {
  /** Play JSON de/serializer for CreateUserReq */
  implicit val format: Format[SignupReq] = Json.format
}
