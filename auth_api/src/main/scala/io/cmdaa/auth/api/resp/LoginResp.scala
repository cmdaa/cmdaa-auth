package io.cmdaa.auth.api.resp

import io.cmdaa.auth.api.SessionToken
import play.api.libs.json.{Format, Json}

/** Structure for Login responses */
case class LoginResp(token: SessionToken)

object LoginResp {
  /** Play Json de/serializer for LoginResp */
  implicit val format: Format[LoginResp] = Json.format
}
