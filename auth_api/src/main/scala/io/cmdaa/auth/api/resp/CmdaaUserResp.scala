package io.cmdaa.auth.api.resp

import io.cmdaa.auth.api.CmdaaUser
import play.api.libs.json.{Format, Json}

/** Structure for CmdaaUser responses */
case class CmdaaUserResp(user: CmdaaUser)

object CmdaaUserResp {
  /** Play JSON de/serializer */
  implicit val format: Format[CmdaaUserResp] = Json.format
}
