package io.cmdaa.auth.api.resp

import io.cmdaa.auth.api.PkiUserInfo
import play.api.libs.json.{Format, Json}

/**
 * Unregistered PKI Response.
 *
 * Emitted when a PKI authenticated user attempts to login, but has not yet
 * registered (ie - their s_dn/i_dn, email, username, etc.. aren't found in the
 * system).
 *
 * Optional information is meant to seed the signup form with information that
 * was found, either in the DN itself, or from an implementation of the CMDAA
 * User Auth Plugin.
 *
 * @param userInfo derived/parsed/found information for a user
 */
case class UnregisteredPkiResp(userInfo: PkiUserInfo)

object UnregisteredPkiResp {
  /** Play Json de/serializer for UnregisteredPkiResp */
  implicit val format: Format[UnregisteredPkiResp] = Json.format
}
