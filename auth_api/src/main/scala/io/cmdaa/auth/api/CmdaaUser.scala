package io.cmdaa.auth.api

import play.api.libs.json.{Format, Json}

/** Encapsulates user data sent to from the CMDAA-Auth API. */
case class CmdaaUser (
  id:        String,
  username:  String,
  email:     String,
  firstName: String,
  lastName:  String,
  meta:      UserMeta
)
object CmdaaUser {
  implicit val format: Format[CmdaaUser] = Json.format
}

/** Encapsulates user metadata sent to from the CMDAA-Auth API. */
case class UserMeta (
  updatedBy: String,
  created:   String,
  updated:   String,
  schema: Int = 1
)
object UserMeta {
  implicit val format: Format[UserMeta] = Json.format
}
