package io.cmdaa.auth.api

import play.api.libs.json.{Format, Json}

/**
 * Container for PkiUserInfo. Contains the s_dn/i_dn and whatever other
 * pertinent user information that could be derived from parsing or service
 * requests.
 *
 * @param s_dn subject DN
 * @param i_dn issuer DN
 *
 * @param username  parsed/derived/found username, if any
 * @param email     parsed/derived/found email, if any
 * @param firstName parsed/derived/found first name, if any
 * @param lastName  parsed/derived/found last name, if any
 */
case class PkiUserInfo (
  s_dn: String,
  i_dn: String,

  username:  Option[String] = None,
  email:     Option[String] = None,
  firstName: Option[String] = None,
  lastName:  Option[String] = None
)
object PkiUserInfo {
  /** Play Json de/serializer for PkiUserInfo */
  implicit val format: Format[PkiUserInfo] = Json.format
}
