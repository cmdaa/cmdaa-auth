import play.sbt.routes.RoutesKeys
import sbt.Keys.version
import sbtcrossproject.CrossPlugin.autoImport.{CrossType, crossProject}

val nexus = "https://52.72.213.202/repository/maven-"
val playVer = "2.8.8"

lazy val scala212 = "2.12.12"
lazy val scala213 = "2.13.3"
lazy val supportedScalaVersions = List(scala212, scala213)

ThisBuild / organization := "io.cmdaa.auth"
ThisBuild / version      := "0.0.23-SNAPSHOT"
ThisBuild / scalaVersion := scala213

ThisBuild / publishTo := {
  if (isSnapshot.value)
    Some("CMDAA Snapshots Nexus" at nexus+"snapshots")
  else
    Some("CMDAA Releases Nexus" at nexus+"releases")
}

ThisBuild / resolvers ++= Seq (
  Resolver.mavenLocal,
  "CMDAA Nexus" at nexus+"public/"
)

lazy val root = (project in file("."))
  .aggregate(auth_api.jvm, auth_api.js, auth_pki, auth_dao, auth_service)
  .settings(
    name := "cmdaa-auth",
    publish / skip := true,
    crossScalaVersions := Nil
  )

lazy val auth_api = crossProject(JSPlatform, JVMPlatform)
  .crossType(CrossType.Pure)
  .in(file("auth_api"))
  .settings(
    name := "cmdaa-auth-api",

    libraryDependencies ++= Seq(
      "com.typesafe.play" %%% "play-json"            % "2.9.2",
      "com.beachape"      %%% "enumeratum-play-json" % "1.6.1"
    ),

    crossScalaVersions := Nil
  )

lazy val auth_pki = (project in file("auth_pki"))
  .dependsOn(auth_api.jvm)
  .settings(
    name := "cmdaa-auth-pki",

    libraryDependencies ++= Seq(
      "com.typesafe.play" %% "play-guice" % "2.8.8"
    ),

    crossScalaVersions := Nil
  )

lazy val auth_dao = (project in file("auth_dao"))
  .settings(
    name := "cmdaa-auth-dao",

    ThisBuild / scalafixDependencies +=
      "org.scala-lang.modules" %% "scala-collection-migrations" % "2.4.4",

    libraryDependencies ++= Seq(
      "javax.inject" % "javax.inject" % "1",

      "org.reactivemongo"      %% "reactivemongo"           % "1.0.7",
      "org.scala-lang.modules" %% "scala-collection-compat" % "2.5.0"
    ),

    crossScalaVersions := supportedScalaVersions,
    addCompilerPlugin(scalafixSemanticdb),
    scalacOptions ++= Seq("-Yrangepos", "-P:semanticdb:synthetics:on")
  )

lazy val auth_service = (project in file("auth_service"))
  .enablePlugins(PlayScala)
  .dependsOn(auth_api.jvm, auth_pki, auth_dao)
  .settings(
    name := "cmdaa-auth-service",

    libraryDependencies ++= Seq(guice, ws,
      "org.reactivemongo"      %% "play2-reactivemongo" % "1.0.4-play28",
      "org.scalatestplus.play" %% "scalatestplus-play"  % "5.1.0" % "test"
    ),

    publish / skip := true,
    crossScalaVersions := Nil,

    RoutesKeys.routesImport ++= Seq(
      "play.modules.reactivemongo.PathBindables._",
      "reactivemongo.api.bson.BSONObjectID"
    )
  )
