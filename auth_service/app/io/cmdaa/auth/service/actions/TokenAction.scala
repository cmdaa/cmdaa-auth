package io.cmdaa.auth.service.actions

import play.api.http.HeaderNames
import play.api.mvc.Results.Unauthorized
import play.api.mvc._

import javax.inject.Inject
import scala.concurrent.{ExecutionContext, Future}

class TokenAction @Inject() (
  val parser: BodyParsers.Default
)(implicit
  protected val executionContext: ExecutionContext
) extends ActionBuilder[TokenedRequest, AnyContent] {
  import TokenAction.r_headerToken

  override def invokeBlock[A] (
    request: Request[A],
    block: TokenedRequest[A] => Future[Result]
  ): Future[Result] = {
    extractBearerToken(request) match {
      case Some(token) =>
        block(new TokenedRequest(token, request))
      case None => // No Token presented
        Future successful Unauthorized
    }
  }

  private def extractBearerToken[A](request: Request[A])
  : Option[String] =
    request.headers.get(HeaderNames.AUTHORIZATION) collect {
      case r_headerToken(token) => token
    }
}

object TokenAction {
  /** Regex for parsing Bearer Tokens from the "Authorization" Header */
  private val r_headerToken = """Bearer (.+?)""".r
}

class TokenedRequest[A] (
  val token: String,
  request: Request[A]
) extends WrappedRequest[A](request)
