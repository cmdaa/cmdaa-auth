package io.cmdaa.auth.service.controllers.api01

import akka.actor.ActorSystem
import io.cmdaa.auth.api.PkiUserInfo
import io.cmdaa.auth.api.req.{LoginReq, SignupReq}
import io.cmdaa.auth.api.resp.{CmdaaUserResp, LoginResp, UnregisteredPkiResp}
import io.cmdaa.auth.dao.{CmdaaUserDao, SessionTokenDao}
import io.cmdaa.auth.model
import io.cmdaa.auth.model.SessionToken
import io.cmdaa.auth.pki.PkiAuthenticator
import io.cmdaa.auth.service.actions.TokenAction
import io.cmdaa.auth.service.dao._
import io.cmdaa.auth.util.Hash
import play.api.Logging
import play.api.libs.json._
import play.api.mvc._
import reactivemongo.api.bson.BSONObjectID

import javax.inject.{Inject, Singleton}
import scala.collection.{Seq => scSeq}
import scala.concurrent.{ExecutionContext, Future}
import scala.concurrent.duration._

@Singleton
class CmdaaUserController @Inject() (
  cc:          ControllerComponents,
  tokenAction: TokenAction,
  usrDao:      CmdaaUserDao,
  tknDao:      SessionTokenDao,
  pkiAuth:     PkiAuthenticator,
  actorSystem: ActorSystem
)(implicit ec: ExecutionContext)
  extends AbstractController(cc)
    with Logging
{
  // TODO: make configurable
  private val expireMillis: Long = 360000 //0

  /** The name of the user collection */
  private val colName = usrDao.colName

  usrDao.initCollection() foreach { _ =>
    logger.debug(s"Collection '$colName': configured")
  }

  actorSystem.scheduler.scheduleAtFixedRate(10.seconds, 10.minutes) { () =>
    tknDao.clearExpired(expireMillis) foreach { wr =>
      logger.debug(s"Cleaned up ${wr.n} expired session tokens.")
    }
  }

  /** Action for route: POST '/api/v1/login' */
  def login(): Action[JsValue] =
    Action.async(parse.json) { req =>
      logger.debug(s"POST '/api/v1/login'")

      req.body.validate[LoginReq]
        .fold(f_jsErrResp, loginReq => {
          val username = loginReq.username

          usrDao.getPhash(username) flatMap {
            _.fold(f_loginFailed(username)) { userPhash =>
              if (Hash.check(loginReq.password, userPhash.phash))
                issueToken(username, userPhash._id)
              else
                f_loginFailed(username)
            }
          }
        })
    }

  /** Action for route: GET '/api/v1/login' */
  def loginPki(): Action[AnyContent] = {
    Action.async(parse.default) { req =>
      logger.debug(s"GET '/api/v1/login'")

      (for {
        ver  <- req.headers.get("ssl-client-verify")
        s_dn <- req.headers.get("ssl-client-subject-dn")
        i_dn <- req.headers.get("ssl-client-issuer-dn")
      } yield {
        if (ver.equalsIgnoreCase("SUCCESS")) {
          usrDao.findByDn(s_dn, i_dn) flatMap {
            _.fold {
              pkiAuth.userInfo(s_dn, i_dn) flatMap { userInfo =>
                userInfo.email.fold {
                  Future successful Option.empty[model.CmdaaUser]
                } {
                  usrDao.findByEmail(_)
                } flatMap {
                  _.fold {
                    userInfo.username.fold {
                      Future successful Option.empty[model.CmdaaUser]
                    } { usrDao.findByUsername(_) }
                  } { Future successful Some(_) }
                } flatMap {
                  _.fold {
                    Future successful resPkiNotFound(userInfo)
                  } { user =>
                    if (user.s_dn.nonEmpty && user.i_dn.nonEmpty) {
                      Future successful resInvalidCert(s_dn, i_dn, ver)
                    } else {
                      usrDao.updateDn(user._id, s_dn, i_dn) flatMap {
                        _.fold {
                          Future successful resInvalidCert(s_dn, i_dn, ver)
                        } { user => issueToken(user.username, user._id) }
                      }
                    }
                  }
                }
              }
            } { user => issueToken(user.username, user._id) }
          }
        } else { Future successful resInvalidCert(s_dn, i_dn, ver) }
      }) getOrElse { Future successful resNoCert() }
    }
  }

  /** Action for route: POST '/api/v1/users' */
  def signup(): Action[JsValue] =
    Action.async(parse.json) { req =>
      logger.debug(s"POST 'users' - User creation requested.")

      req.body.validate[SignupReq].fold(f_jsErrResp, insReq => {
        (for {
          ver  <- req.headers.get("ssl-client-verify")
          s_dn <- req.headers.get("ssl-client-subject-dn")
          i_dn <- req.headers.get("ssl-client-issuer-dn")
        } yield {
          if (ver.equalsIgnoreCase("SUCCESS")) {
            val usr = req2newUsr(insReq, Some(s_dn), Some(i_dn))
            val usrId = usr._id.stringify

            usrDao.insertPki(usr) flatMap { _ =>
              logger.info(s"  users/$usrId - Inserted")
              fetchUser(usr._id)
            }
          } else {
            Future successful resInvalidCert(s_dn, i_dn, ver)
          }
        }) getOrElse {
          insReq.password.fold {
            Future successful resNoPassword()
          } { password =>
            val usr = req2newUsr(insReq)
            val usrId = usr._id.stringify
            val phash = Hash.saltedHash(password)

            usrDao.insert(usr, phash) flatMap { _ =>
              logger.info(s"  users/$usrId - Inserted")
              fetchUser(usr._id)
            }
          }
        }
      })
    }

  /** Action for route: GET '/api/v1/auth' */
  def auth(): Action[AnyContent] =
    tokenAction.async { req =>
      logger.debug(s"Authenticating user by token: ${req.token}")

      tknDao.access(req.token, expireMillis) flatMap {
        _.fold {
          logger.info(s"SessionToken not found for: ${req.token}")
          Future.successful(res401(req.token))
        } { sessToken =>
          usrDao.findById(sessToken.userId) map {
            _.fold {
              logger.info (
                s"User: '${sessToken.userId.stringify}' not found for "
                  + s"SessionToken: '${sessToken._id.stringify}'")
              res401(req.token)
            }(res200User)
          }
        }
      }
    }

  /** Action for route: DELETE '/api/v1/login' */
  def logout(): Action[AnyContent] =
    tokenAction.async { req =>
      logger.debug(s"User logout by token: ${req.token}")

      tknDao.invalidate(req.token) map { wr =>
        if (wr.n != 0) {
          logger.debug(s"  login/${req.token} - Deleted.")
          Ok(Json.obj("id" -> req.token, "message" -> "deleted"))
        } else {
          logger.warn (
            s"NOT_FOUND. Logout - No tokens removed for: '${req.token}'")

          // Not secure to return a proper error message here. Either way, the
          // token no longer exists. So we return 200.
          Ok(Json.obj("id" -> req.token, "message" -> "deleted"))
        }
      }
    }

  /** Fetches a CmdaaUser by ID; serializes to a Result */
  private def fetchUser(bsonUsrId: BSONObjectID)
    (implicit ec: ExecutionContext)
  : Future[Result] = {
    val usrId = bsonUsrId.stringify

    usrDao.findById(bsonUsrId) map { o_usr =>
      o_usr.fold {
        logger.warn(s"NOT_FOUND. Collection '$colName'; ID '$usrId'")
        res404(usrId)
      } { usr =>
        logger.info(s"Collection '$colName' - GET 'users/$usrId'")
        res200User(usr)
      }
    }
  }

  /** Generate and store a SessionToken for the user. */
  private def issueToken(username: String, userId: BSONObjectID)
    (implicit ec: ExecutionContext)
  : Future[Result] = {
    val tkn = SessionToken.genToken(userId)
    logger.info(s"  login by '$username'. Issue token '${tkn.token}'")

    tknDao.insert(tkn) map { wr =>
      if (wr.n != 0) {
        Ok(Json toJson LoginResp(sessTkn2api(tkn)))
      } else {
        logger.warn(s"Error creating token. '${tkn.token}' for '$username'")
        InternalServerError
      }
    }
  }

  /** Async login failed response with logging */
  private def f_loginFailed(username: String): Future[Result] = {
    logger.info(s"Attempt to login by '$username' failed.")
    Future.successful(res404(username))
  }

  /** Generic Response for a document can not be found by ID */
  private def res404(id: String): Result =
    NotFound(Json.obj(
      "status" -> "NotFound",
      "code"   -> NOT_FOUND,
      "id"     -> id
    ))

  /** Generic Response for "Unauthorized" reqest */
  private def res401(id: String): Result =
    Unauthorized(Json.obj(
      "status" -> "Unauthorized",
      "code"   -> UNAUTHORIZED, // 401
      "id"     -> id
    ))

  private def resPkiNotFound(userInfo: PkiUserInfo): Result =
    NotFound(Json toJson UnregisteredPkiResp(userInfo))

  /** Response for no client certificate */
  private def resNoCert(): Result =
    Unauthorized(Json.obj(
      "status" -> "Unauthorized",
      "code"   -> UNAUTHORIZED, // 401
      "msg"    -> "No client certificate"
    ))

  /** Response for no client certificate */
  private def resNoPassword(): Result =
    Unauthorized(Json.obj(
      "status" -> "BadRequest",
      "code"   -> BAD_REQUEST, // 400
      "msg"    -> "No password provided"
    ))

  /** Response for invalid client certificate */
  private def resInvalidCert(s_dn: String, i_dn: String, ver: String)
  : Result =
    Forbidden(Json.obj(
      "status" -> "Forbidden",
      "code" -> FORBIDDEN, //403
      "msg"  -> "Invalid Client Certificate",
      "s_dn" -> s_dn,
      "i_dn" -> i_dn,
      "ver"  -> ver
    ))

  /** Respond with a CmdaaLog */
  private def res200User(cmdaaLog: model.CmdaaUser): Result =
    Ok(Json toJson CmdaaUserResp(user2req(cmdaaLog)))

  /** Generic JSON Validation Error Response */
  private def f_jsErrResp (
    errors: scSeq[(JsPath, scSeq[JsonValidationError])]
  ): Future[Result] = Future.successful {
    BadRequest(Json.obj("message" -> JsError.toJson(errors)))
  }
}
