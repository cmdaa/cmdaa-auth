package io.cmdaa.auth.service.dao

import io.cmdaa.auth.dao.MongoComponents
import play.modules.reactivemongo.ReactiveMongoApi
import reactivemongo.api.{AsyncDriver, DB, MongoConnection}

import javax.inject.Inject
import scala.concurrent.Future

/**
 * Play Implementation of MongoComponents which delegates to the
 * ReactiveMongoApi from the play2-reactivemongo plugin.
 *
 * @param reactiveMongoApi the ReactiveMongoApi to delegate to.
 */
class PlayMongoComponents @Inject() (
  private val reactiveMongoApi: ReactiveMongoApi
) extends MongoComponents {
  override def asyncDriver: AsyncDriver     = reactiveMongoApi.asyncDriver
  override def connection:  MongoConnection = reactiveMongoApi.connection
  override def database:    Future[DB]      = reactiveMongoApi.database
}
