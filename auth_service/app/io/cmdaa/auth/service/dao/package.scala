package io.cmdaa.auth.service

import io.cmdaa.auth.api.req.SignupReq
import io.cmdaa.auth.{api, model}
import reactivemongo.api.bson.BSONDateTime

import java.text.SimpleDateFormat
import java.util.Date

package object dao {
  /** CreateUserReq -> model.CmdaaUser */
  def req2newUsr (
    req: SignupReq,
    s_dn: Option[String] = None,
    i_dn: Option[String] = None
  ): model.CmdaaUser =
    model.CmdaaUser.newUser (
      req.username, req.email, req.firstName, req.lastName, s_dn, i_dn)

  /** model.CmdaaUser -> api.CmdaaUser */
  def user2req(usr: model.CmdaaUser): api.CmdaaUser =
    api.CmdaaUser(usr._id.stringify, usr.username, usr.email,
      usr.firstName, usr.lastName, meta2req(usr.meta))

  /** model.SessionToken -> api.SessionToken */
  def sessTkn2api(tkn: model.SessionToken): api.SessionToken =
    api.SessionToken (
      tkn._id.stringify,
      tkn.userId.stringify,
      bdt2dts(tkn.lastAccess),
      tkn.token
    )

  /** model.UserMeta -> api.UserMeta */
  def meta2req(meta: model.UserMeta): api.UserMeta =
    api.UserMeta (
      meta.updatedBy.stringify,
      bdt2dts(meta.created),
      bdt2dts(meta.updated)
    )

  /**
   * Java DateFormatter isn't thread safe; this creates a preconfigured
   * DateFormatter as needed.
   *
   * @return a preconfigued DateFormatter
   */
  private def dateFormatter =
    new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSSZ")

  /**
   * Converts a BSONDateTime to ISO 8601 version of the W3C XML Schema
   * DateTime.
   *
   * @param bdt BSON DateTime
   * @return a standards formatted String
   */
  private def bdt2dts(bdt: BSONDateTime): String =
    dateFormatter.format(new Date(bdt.value))
}
