package io.cmdaa.auth.service.modules

import com.google.inject.AbstractModule
import io.cmdaa.auth.dao.MongoComponents
import io.cmdaa.auth.service.dao.PlayMongoComponents

/**
 * Tells Guice the implementation we're using for the MongoComponents trait.
 */
class PlayMongoModule extends AbstractModule {
  override def configure(): Unit =
    bind(classOf[MongoComponents])
      .to(classOf[PlayMongoComponents])
}
