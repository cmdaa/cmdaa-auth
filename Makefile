IMAGE	:= cmdaa/cmdaa-auth
TAG	:= 0.0.23

image:
	docker build \
	  --file Dockerfile.build \
	  --tag  $(IMAGE):$(TAG) \
	    .
	docker tag  $(IMAGE):$(TAG) $(IMAGE):latest

push:
	docker push $(IMAGE):$(TAG)
	docker push $(IMAGE):latest
