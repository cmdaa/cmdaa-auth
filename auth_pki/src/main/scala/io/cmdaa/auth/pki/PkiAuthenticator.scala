package io.cmdaa.auth.pki

import io.cmdaa.auth.api.PkiUserInfo

import scala.concurrent.Future

/**
 * Acquires user info for the given SubjectDN and IssuerDN. Meant to be
 * overridden on a per-deployment basis with code meant to correctly parse,
 * or look up user information from certificates issued from trusted
 * Certificate Authorities.
 */
trait PkiAuthenticator {
  /**
   * Parse or look up user information
   *
   * @param s_dn the subject dn
   * @param i_dn the issuer dn
   * @return async info for an unregistered user
   */
  def userInfo(s_dn: String, i_dn: String): Future[PkiUserInfo]
}

/**
 * Default implementation of the PkiAuthenticator. Simply parses the given
 * SubjectDN and attempts to extract email address and username.
 */
class DefaultPkiAuthenticator extends PkiAuthenticator {
  private val EmailRE    = """(?i)emailAddress=([^,]*)[,$]""".r.unanchored
  private val UsernameRE = """(?i)CN=([^,]*)[,$]""".r.unanchored

  /**
   * Assumes that the CN is a username. Attempts to extract email address and
   * username from the SubjectDN.
   *
   * @param s_dn the subject dn
   * @param i_dn the issuer dn
   * @return async info for an unregistered user
   */
  override def userInfo(s_dn: String, i_dn: String)
  : Future[PkiUserInfo] = {
    val o_email = s_dn match {
      case EmailRE(email) => Some(email)
      case _ => None
    }

    val o_username = s_dn match {
      case UsernameRE(username) => Some(username)
      case _ => None
    }

    Future successful PkiUserInfo(s_dn, i_dn,
      username = o_username,
      email = o_email
    )
  }
}
