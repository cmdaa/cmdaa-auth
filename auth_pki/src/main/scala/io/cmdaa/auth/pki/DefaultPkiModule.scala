package io.cmdaa.auth.pki

import com.google.inject.AbstractModule

/**
 * Tell Guice the implementation we're using for PkiAuthenticator. Easy to swap
 * out with application.conf
 */
class DefaultPkiModule extends AbstractModule {
  override def configure(): Unit =
    bind(classOf[PkiAuthenticator])
      .to(classOf[DefaultPkiAuthenticator])
}
