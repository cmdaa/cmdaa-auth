#!/bin/bash

TAG=0.0.23

sbt dist

eval $(minikube docker-env)
docker build -t cmdaa/cmdaa-auth:${TAG} .

kubectl -n cmdaa rollout restart deploy cmdaa-auth cmdaa-ui
exec kubectl -n cmdaa get pods --watch
