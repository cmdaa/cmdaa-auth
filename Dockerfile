FROM openjdk:11-jre

WORKDIR /svc
COPY auth_service/target/universal/*.zip .

RUN unzip *.zip && mv cmdaa*/* . && rm -rf cmdaa*/ *.zip

ENV MONGODB_URL="mongodb://cmdaa:cmdaa@mongodb:27017/cmdaa"

EXPOSE 80
ENTRYPOINT /svc/bin/cmdaa-auth-service -Dhttp.port=80
